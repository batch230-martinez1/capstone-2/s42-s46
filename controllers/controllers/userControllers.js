const user = require("../models/user.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");
const Product = require("../models/product.js")

// Register user
module.exports.registerUser = (req, res) =>{
	
	let newUser = new user({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		// Syntax: bcrypt.hashSync(dataToBeEncrypted, salt);
			// salt - salt rounds that the bcyrpt alogorithm will run to encrypt the password.
		password: bcrypt.hashSync(req.body.password, 10),
		mobileNumber: req.body.mobileNumber
	})

	console.log(newUser);

	return newUser.save()
	.then(user => {
		console.log(user);
		// send "true" if the user is created
		res.send(true);
	})
	.catch(error =>{
		console.log(error);
		// send "false" if any error is encountered.
		res.send(false);
	})
}
//  Check if email exist
module.exports.checkEmailExists = (req, res) =>{
	return user.find({email: req.body.email}).then(result =>{

		// The result of the find() method returns an array of objects.
			 // we can use array.length method for checking the current result length
		console.log(result);

		// The user already exists
		if(result.length > 0){
			return res.send(true);
			// return res.send("User already exists!");
		}
		// There are no duplicate found.
		else{
			return	res.send(false);
			// return res.send("No duplicate found!");
		}
	})
	.catch(error => res.send(error));
}




// Login user
module.exports.loginUser = (req, res) =>{
	return user.findOne({email: req.body.email})
	.then(result => {
		// User does not exists
		if(result == null){
			return res.send(false);
			// return res.send({message: "No User Found!"});
		}
		// User exists
		else{
			// Syntax: bcrypt.compareSync(data, encrypted)
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

			// If the passwords match/result of the above code is true.
			if(isPasswordCorrect){
				// Generate an access token
				// Uses the "createAccessToken" method defined in the "auth.js" file
				// Returning an object back to the frontend application is common practice to ensure information is properly labeled and real world examples normally return more complex information represented by objects
				return res.send({accessToken: auth.createAccessToken(result)});
			}
			else{
				return res.send(false); // if password do not match
				// return res.send({message: "Incorrect password!"});
			}
		}
	})
}


// Get user details
module.exports.getProfile = (request, response) =>{
  const userData = auth.decode(request.headers.authorization);
  return user.findById(userData.id).then(result =>{
    result.password = "*****";
    response.send(result);
  })    
}

// order checkout
module.exports.createOrder = async (request, response) =>{
  const userData =auth.decode(request.headers.authorization);
  const admin= {isAdmin: auth.decode(request.headers.authorization).isAdmin}
  let productName = await Product.findById(request.body.productId)
  .then(result => result.name);
  let productPrice = await Product.findById(request.body.productId)
  .then(result=> result.price);
  let orderData = {    
    userId: userData.id,
    email: userData.email,
    productId: request.body.productId,
    productName: productName,
    quantity: request.body.quantity,
    productPrice: productPrice 
  }
  console.log(orderData)
  if(admin.isAdmin==true){
   let message = "Admins are not allowed to order products";
   console.log(message);
   response.send(false);
  }
  else{
 let isProductUpdated = await Product.findById(orderData.productId)
 .then(product =>{
  if(product.stocks<=0){
    let message = "Product sold out";
    console.log(message)
    response.send(false);
    return false
  }
  else if(product.isActive==false){
    let message = "Product is unavailable";
    console.log(message)
    response.send(false);
    return false
  }
  else {
    product.stocks = product.stocks - orderData.quantity;
    return product.save()
    .then(result=>{console.log(result);
    return true;
    })
  }
 }) 
 .catch(error=>{
   console.log(error);
   return false;
 })
 console.log(isProductUpdated);
 
 let isUserUpdated = await user.findById(orderData.userId)
 .then(user=>
  {
  if(isProductUpdated==false){
    return false;
  }
  else{
    user.orders.push({
      totalAmount: productPrice*request.body.quantity,
      products: {
        productId: orderData.productId,
        productName: orderData.productName,
        quantity: orderData.quantity
      } 
    });
    return user.save()
    .then(result=>{
      console.log(result);
      return true;
    })
  }
 }).catch(error=>{
  console.log(error);
  return false;
}) 

 console.log(isUserUpdated);
 (isUserUpdated && isProductUpdated) ? response.send(true) : Response.send(false)
}
}





// stretch goal

// set user as admin
 module.exports.setUserToAdmin= (userId, data) =>{
  if(data.isAdmin== true){
    return user.findByIdAndUpdate(userId,
      {
        isAdmin: data.user.isAdmin
      },
      {
        new: true
      })
      .then((result,error)=>{
        if(error){
          return false;
        }
        else{
          return result;
        }
      })
  }
  else{
    let message = Promise.resolve(`User must be ADMIN to access this functionality`);
    console.log(message);
    return false;
  }
}

// Retrieve authenticated user’s orders
module.exports.getUsersOrder = (request, response) =>{
  return user.findById(request.params.userId).then(result =>{
    let usersOrder = result.orders;
    console.log(result);
    response.send(usersOrder);
  })
  .catch(error=>{
    response.send(error);
  })
}


  module.exports.getAllOrders = (request, response) =>{
    const data = {
      isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    if(data.isAdmin == true){
   return user.find().then(users =>{
      let allOrders = [];
      for(let user of users){
        allOrders.push(...user.orders);
      }
      response.send(allOrders);
    })
    .catch(error=>{
      response.send(error);
    })
  }
  else{
    // let message = Promise.resolve(`User must be ADMIN to access this functionality`);
    return false;
  }
  }

  // Get all users
  module.exports.getAllUsers = (req, res) =>{
	
    const userData = auth.decode(req.headers.authorization);
  console.log(userData.isAdmin);
    if(userData.isAdmin){
      return user.find({}).then(result => res.send(result));
    }
    else{
      return res.send(false);
      // return res.status(401).send("You don't have access to this page!");
    }
  }











