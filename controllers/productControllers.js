const mongoose = require("mongoose");
const Product = require("../models/product.js");
const auth = require("../auth.js")

//  Create Product
module.exports.addProduct = (reqBody) =>{
  if(reqBody.isAdmin == true){  
    let newProduct = new Product({
    name: reqBody.product.name,
    description: reqBody.product.description,
    price: reqBody.product.price,
    stocks: reqBody.product.stocks
  })

  return newProduct.save().then((newProduct, error)=>
  {
    if(error){
        return error;
    }
    else{
      return newProduct;
    }
  })
}
else{
  let message = Promise.resolve('User must be ADMIN to access this functionality');
  return message;
}  
}

// Get all products

module.exports.getAllProducts = (req, res) =>{
	
	// const userData = auth.decode(req.headers.authorization);

	if(req.userData.isAdmin){
		return Product.find({}).then(result => res.send(result));
	}
	else{
		return res.send(false);
		// return res.status(401).send("You don't have access to this page!");
	}
}

// Get all active products
module.exports.getActiveProducts = () => {
  return Product.find({isActive: true}).then(result =>{
    return result;
  })
}

// retrieve single product
module.exports.getProduct= (req, res) =>{
	console.log(req.params.productId);

	return Product.findById(req.params.productId).then(result => res.send(result));
}

//  update a product
module.exports.updateProduct = (productId, newData) => {
  if(newData.isAdmin == true){
    return Product.findByIdAndUpdate(productId,
      {
        name: newData.product.name, 
        description: newData.product.description,
        price: newData.product.price,
        stocks: newData.product.stocks
      },
      {new: true}
    ).then((result, error)=>{
      if(error){
        return error;
      }
      return result
    }) 
  }
  else{
    let message = Promise.resolve('User must be ADMIN to access this functionality');
    return message;
  }
}


//  archive product
module.exports.archiveProduct = (productId, archive) =>{
  if(archive.isAdmin == true){

    return Product.findByIdAndUpdate(productId, 
      {
        isActive : archive.product.isActive
      },
      {
        new : true
      })
    .then((result, error)=>{
      if(error){
          return false;
        }
     else{
      return result
     }
    }
    )
    }
    else{
      let message = Promise.resolve('User must be ADMIN to access this functionality');
      return message.then((value) => {return value});
    }
}