const express = require("express");
const userControllers = require("../controllers/userControllers.js");
const auth = require("../auth.js")
const router = express.Router();

// register route
router.post("/register", userControllers.registerUser);



// check email route
router.post("/checkEmail", userControllers.checkEmailExists);


// login route
router.post("/login", userControllers.loginUser);

// get user details route
router.get("/details", auth.verify, userControllers.getProfile);

// checkout route
router.post("/checkout", auth.verify, userControllers.createOrder);



// Stretch goal 
  
// set User to Admin
router.patch("/:userId/toAdmin", auth.verify,(request,response)=> {
  const newData = {
  user: request.body, 	

  isAdmin: auth.decode(request.headers.authorization).isAdmin
}


  userControllers.setUserToAdmin(request.params.userId, newData).then(resultFromController => {
  response.send(resultFromController)
})
})

// Retrieve authenticated user orders
router.get("/:userId/userOrders", auth.verify, userControllers.getUsersOrder);

// retrieve all orders
router.get("/allOrders", auth.verify, userControllers.getAllOrders);

// retrieve all users
router.get("/all", auth.verify, userControllers.getAllUsers);

module.exports = router;